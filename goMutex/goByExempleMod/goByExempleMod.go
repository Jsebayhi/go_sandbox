package main

import (
    "fmt"
    "sync"
    "sync/atomic"
    "time"
)

func main() {

    // For our example the `state` will be a map.
    var nbIt = 10
    var state = make([]int, 0)

    // This `mutex` will synchronize access to `state`.
    var mutex = &sync.Mutex{}

    // We'll keep track of how many read and write
    // operations we do.
    var readOps uint64 = 0

    // Here we start nbIt goroutines to execute repeated
    // reads against the state, once per millisecond in
    // each goroutine.
    for r := 0; r < nbIt; r++ {
        fmt.Println(r)
        go func() {

            // For each read we pick a key to access,
            // `Lock()` the `mutex` to ensure
            // exclusive access to the `state`, read
            // the value at the chosen key,
            // `Unlock()` the mutex, and increment
            // the `readOps` count.
            mutex.Lock()
            state = append(state, r)
            mutex.Unlock()
            atomic.AddUint64(&readOps, 1)

            // Wait a bit between reads.
            time.Sleep(time.Millisecond)
        }()
    }

    // With a final lock of `state`, show how it ended up.
    mutex.Lock()
    fmt.Println("state:", state)
    mutex.Unlock()
}
