package main

import (
	"fmt"
)

type Struct struct {
	name_last string
	funcPtr   func(msg string)
}

func print(msg string) {
	fmt.Println(msg)
}

func main() {

	Struct := Struct{"family", print}

	Struct.funcPtr("toto")
}
