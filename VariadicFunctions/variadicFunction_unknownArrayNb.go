// [_Variadic functions_](http://en.wikipedia.org/wiki/Variadic_function)
// can be called with any number of trailing arguments.
// For example, `fmt.Println` is a common variadic
// function.

package main

import "fmt"

// Here's a function that will take an arbitrary number
// of `ints` as arguments.
func test(url string, params ...[2]string) {
	fmt.Println("url: " + url)
	for _, param := range params {
		fmt.Println(param)
	}
}

func main() {

	// Variadic functions can be called in the usual way
	// with individual arguments.
	fmt.Println("test 1")
	test("url")
	fmt.Println("\ntest 2")
	test("url", [2]string{"str1", "toto"}, [2]string{"str1", "tata"}, [2]string{"str1", "tutu"})
}
