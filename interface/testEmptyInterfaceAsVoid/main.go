package main

import (
	"fmt"
)

type S_struct struct {
	name string
}

func testCase(emp interface{}) {

	fmt.Printf("obj name:%+v\n", emp)
}

func main() {

	s_struct := S_struct{"toto"}

	testCase(s_struct)

}
