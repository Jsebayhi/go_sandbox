package testInterface

import (
	"fmt"
	"reflect"
)

func goReflectFunc(obj interface {}, apply func(a interface{}) {

	fmt.Printf("goReflectFunc")
}

func main() {

	var a string

	a = "toto"
	goReflect(a)
}
