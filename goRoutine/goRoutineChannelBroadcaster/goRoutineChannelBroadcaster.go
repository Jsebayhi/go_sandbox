package main

import (
	"log"
	"sync"
	"time"
)

func BroadcastSync(waitGroup sync.WaitGroup, inputChannel chan interface{}, receivers *map[string]chan interface{}, receiversLocker sync.Locker) {
		waitGroup.Add(1)
		for in := range inputChannel {
			forwardWaitGroup := sync.WaitGroup{}
			for id, forwardChannel := range *receivers {
				log.Printf("Broadcasting %v to subscriber %s.", in, id)
				go func (forwardChannel chan interface{}, in interface{}) {
					forwardWaitGroup.Add(1)
					forwardChannel <- in
					forwardWaitGroup.Done()
				}(forwardChannel, in)
			}
			forwardWaitGroup.Wait()
		}
		waitGroup.Done()
}

func Receiver(waitgroup sync.WaitGroup, id int, channel chan interface{}, waitDuration time.Duration, backChannel chan interface{}) {
	waitgroup.Add(1)
	for got := range channel {
		log.Printf("Receiver %d - received: %d.", id, got)
		backChannel <- got
		time.Sleep(waitDuration)
	}
	waitgroup.Done()
}

func main() {
	waitgroup := sync.WaitGroup{}
	channel := make(chan interface{})
	BackChannel := make(chan interface{})
	receiverLocker := &sync.Mutex{}
	receivers := map[string]chan interface{}{}
	receivers["1"] = make(chan interface{})
	receivers["2"] = make(chan interface{})

	go Receiver(waitgroup, 1, receivers["1"], 1 * time.Second, BackChannel)
	go Receiver(waitgroup, 2, receivers["2"], 3 * time.Second, BackChannel)

	go BroadcastSync(waitgroup, channel, &receivers, receiverLocker)

	channel <- 1
	for range receivers {
		got := <-BackChannel
		log.Printf("Expected %v, got %v.", 1, got)
	}

	channel <- 2
	for range receivers {
		got := <-BackChannel
		log.Printf("Expected %v, got %v.", 1, got)
	}

	receivers["3"] = make(chan interface{})
	go Receiver(waitgroup, 3, receivers["3"], 1 * time.Second, BackChannel)

	channel <- 3
	for range receivers {
		got := <-BackChannel
		log.Printf("Expected %v, got %v.", 1, got)
	}

	waitgroup.Wait()
	log.Print("Job done.")
}
