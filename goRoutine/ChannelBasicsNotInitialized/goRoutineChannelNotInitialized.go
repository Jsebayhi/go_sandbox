package main

import (
	"sync"
	"log"
	"bufio"
	"os"
	"fmt"
	"time"
)

func ListenerBlockOnNil(waitGroup *sync.WaitGroup, id int, input chan interface{}) {
	waitGroup.Add(1)
	log.Printf("Listener %d: waiting input channel report.", id)
	_, isOpen := <-input
	log.Printf("Listener %d: channel status: %v.", id, isOpen)
	waitGroup.Done()
}

func Listener(waitGroup *sync.WaitGroup, id int, input chan interface{}) {
	waitGroup.Add(1)
	if input != nil {
		log.Printf("Listener %d: waiting input channel report.", id)
		_, isOpen := <-input
		log.Printf("Listener %d: channel status: %v.", id, isOpen)
	} else {
		log.Printf("Listener %d: channel nil: %v.", id, input)
	}
	waitGroup.Done()
}

func main() {
	waitGroup := &sync.WaitGroup{}
	var input chan interface{}
	listenerLastId := 0

	for listenerLastId < 5 {
		go Listener(waitGroup, listenerLastId, input)
		listenerLastId++
	}
	time.Sleep(1 * time.Second)

	for listenerLastId < 10 {
		go ListenerBlockOnNil(waitGroup, listenerLastId, input)
		listenerLastId++
	}
	time.Sleep(1 * time.Second)
	//input = make(chan interface{})
	waitGroup.Wait()

	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Enter anything to quit")
	_, _ = reader.ReadString('\n')
}
