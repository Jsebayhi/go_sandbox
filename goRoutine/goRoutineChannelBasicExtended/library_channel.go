package main

import (
	"time"
	"sync"
	"log"
)

type RedirectChannel struct {
	channel         chan interface {}
	timeoutDuration time.Duration
	id int
}

// Redirect input one by one. Speed depends is O(Sum(R1 * RT1, ...)) dependents with R being the number of redirection
//  and RT it's timeout duration.
// quit: channel to use to tell the channelSplitter to exit.
// input: channel to listen to redirect.
// redirects: list of redirections containing a channel and a timeout duration. First in, first redirection tried.
func channelSplitter(quit chan int, input chan interface{}, redirects ...*RedirectChannel) {
	waitgroup := sync.WaitGroup{}
	sleeper := func (backChan chan bool, d time.Duration){
		waitgroup.Add(1)
		time.Sleep(d)
		backChan <- true
		waitgroup.Done()
	}

	splitterLoop:
	for {
		in, status := <-input
		if status != true {
			break splitterLoop
		}
		timeoutChan := make(chan bool)
		redirectsLoop:
		for _, redirect := range redirects {
			log.Printf("Trying redirect %d.", redirect.id)
			go sleeper(timeoutChan, redirect.timeoutDuration)
			select {
			case redirect.channel <- in:
				break redirectsLoop
			case <-timeoutChan:
			case <-quit:
				break splitterLoop
			}
		}
	}
}