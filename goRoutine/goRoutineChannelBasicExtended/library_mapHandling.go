package main

func RemoveFromMap(m map[Person]chan interface{}, rm Person) map[Person] chan interface{} {
	newMap := map[Person]chan interface{}{}
	for person, channel := range m {
		if person != rm {
			newMap[person] = channel
		}
	}
	return newMap
}
