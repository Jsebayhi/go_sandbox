package main

import (
	"testing"
	"time"
	"fmt"
	"sync"
	"log"
)

const (
	ChannelClosed bool = false
)
func TestChannelSplitter(T *testing.T) {
	log.SetFlags(log.Ldate|log.Lmicroseconds)
	waitgroup := sync.WaitGroup{}

	success := true

	redirect1 := RedirectChannel{channel: make(chan interface{}), timeoutDuration: 1 * time.Second, id: 1}
	redirect2 := RedirectChannel{channel: make(chan interface{}), timeoutDuration: 2 * time.Second, id: 2}
	redirect3 := RedirectChannel{channel: make(chan interface{}), timeoutDuration: 3 * time.Second, id: 3}
	inputChannel := make(chan interface{})
	quitChannel := make(chan int)

	inputExpected_Redirect1 := []int{0, 1, 2}
	inputExpected_Redirect2 := []int{3, 5}
	inputExpected_Redirect3 := []int{4, 6}
	inputContent := append(inputExpected_Redirect1, inputExpected_Redirect2...)
	inputContent = append(inputContent, inputExpected_Redirect3...)

	syncRedir1And2 := make(chan bool)
	//redirect1 handler
	go func () {
		waitgroup.Add(1)
		received := []int{}
		loop:
		for i := 0; ; i++ {
			if i == 3 {
				break loop
			}
			got, status := <-redirect1.channel
			if status == ChannelClosed {
				break loop
			}
			log.Printf("redirect1 received %d.", got.(int))
			received = append(received, got.(int))
		}
		log.Print("redirect1 content - received: " + fmt.Sprint(received) + " expected: " + fmt.Sprint(inputExpected_Redirect1))
		if !intSliceComparison(received, inputExpected_Redirect1) {
			success = success && false
		}
		waitgroup.Done()
	}()
	//redirect2 handler
	go func () {
		waitgroup.Add(1)
		received := []int{}
		loop:
		for i := 0; ; i++ {
			select {
			case <-syncRedir1And2:
				got, status := <-redirect2.channel
				if status == ChannelClosed {
					break loop
				}
				log.Printf("redirect2 received %d.", got.(int))
				received = append(received, got.(int))
				syncRedir1And2 <- true
			case <-quitChannel:
				break loop
			}
		}
		log.Print("redirect2 content - received: " + fmt.Sprint(received) + " expected: " + fmt.Sprint(inputExpected_Redirect2))
		if !intSliceComparison(received, inputExpected_Redirect2) {
			success = success && false
		}
		waitgroup.Done()
	}()
	//redirect3 handler
	go func () {
		waitgroup.Add(1)
		received := []int{}
		syncRedir1And2 <- true
		loop:
		for i := 0; ; i++ {
			select {
			case <-syncRedir1And2:
				got, status := <-redirect3.channel
				if status == ChannelClosed {
					break loop
				}
				log.Printf("redirect3 received %d.", got.(int))
				received = append(received, got.(int))
				syncRedir1And2 <- true
			case <-quitChannel:
				break loop
			}
		}
		log.Print("redirect3 content - received: " + fmt.Sprint(received) + " expected: " + fmt.Sprint(inputExpected_Redirect3))
		if !intSliceComparison(received, inputExpected_Redirect3) {
			success = success && false
		}
		waitgroup.Done()
	}()

	go channelSplitter(quitChannel, inputChannel, &redirect1, &redirect2, &redirect3)
	//input feeder
	for input := range inputContent {
		log.Printf("Sending input: %d.", input)
		inputChannel <- input
	}
	time.Sleep(3 * time.Second)
	quitChannel <- 0
	close(redirect1.channel)
	close(redirect2.channel)
	close(redirect3.channel)
	waitgroup.Wait()
	if success == false {
		log.Fatal("failed.")
	}
}

func intSliceComparison(s1, s2 []int) bool {
	if len(s1) != len(s2) {
		return false
	}
	for i := 0; i < len(s1); i++ {
		if s1[i] != s2[i] {
			return false
		}
	}
	return true
}