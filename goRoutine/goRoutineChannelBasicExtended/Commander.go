package main

import (
	"time"
	"log"
	"math/rand"
)

// will give orders to people.
func Commander(addressBook AddressBook) {
	waitgroup.Add(1)
	defer waitgroup.Done()
	life:
	for {
		time.Sleep(3 * time.Second)
		if len(addressBook.Addresses()) == 1 {
			for _, address := range addressBook.Addresses() {
				address <- Die{}
			}
		}
		if len(addressBook.Addresses()) == 0 {
			log.Println("Commander reporting: Not enough people alive. Stopping.")
			break life
		}
		sender, senderChannel := pickTarget(addressBook.Addresses())
		target, _ := pickTarget(RemoveFromMap(addressBook.Addresses(), sender))
		log.Printf("Commander reporting: Commanding %s to send a message to %s.", sender.String(), target.String())
		senderChannel <- Order{contact: target}
	}
}

func	pickTarget(addresses map[Person]chan interface{}) (Person, chan interface{}) {
	personId := rand.Intn(len(addresses))
	i := 0
	for person, address := range addresses {
		if i == personId {
			return person, address
		}
		i++
	}
	log.Panic(i, personId)
	return Person(""), nil
}
