package main

import (
	"log"
)

type Person string
func (person Person) String() string {
	return string(person)
}

func PersonSoul(name string, addressBook AddressBook) {
	waitgroup.Add(1)
	mySelf := Person(name)
	myChannel := addressBook.Register(mySelf)
	defer func () {
		addressBook.UnRegister(mySelf)
		waitgroup.Done()
	}()
	life:
	for {
		message := <-myChannel
		switch message.(type) {
		case Order:
			outsideChannel := addressBook.Address(message.(Order).contact)
			msg := Mail{From: mySelf, To: message.(Order).contact}
			log.Printf("Person reporting: %s. Sending message to %s.", mySelf.String(), msg.To)
			outsideChannel <- msg
		case Mail:
			log.Printf("Person reporting: %s. Received message from %s.", mySelf.String(), message.(Mail).From.String())
		case Die:
			log.Printf("Person reporting: %s. Stopping.", mySelf.String())
			break life
		}
	}
}
