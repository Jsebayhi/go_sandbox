package main

import (
	"sync"
	"log"
	"strconv"
	"bufio"
	"os"
	"fmt"
)

const (
	defaultNbPlayer int = 10
)
var (
	waitgroup sync.WaitGroup
)

// Core
func init() {
	log.SetFlags(log.Ldate|log.Lmicroseconds)
}

func main() {
	var playerNb int
	if len(os.Args) == 1 {
		playerNb = defaultNbPlayer
	} else if len(os.Args) == 2 {
		var err error
		playerNb, err = strconv.Atoi(os.Args[1])
		if err != nil {
			log.Print("Parameter must be a valid integer.")
			return
		}
	} else {
		log.Print("Invalid parameter number.")
		return
	}
	addressBook := AddressBookNew()
	for i := 0; i < playerNb; i++ {
		go PersonSoul("player"+strconv.Itoa(i), addressBook)
	}
	go Commander(addressBook)
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Enter anything to quit")
	_, _ = reader.ReadString('\n')
	for _, address := range addressBook.Addresses() {
		address <- Die{}
	}
	waitgroup.Wait()
}