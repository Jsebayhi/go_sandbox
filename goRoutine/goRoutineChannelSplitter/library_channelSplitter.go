package main

import (
	"time"
	"sync"
	"log"
)

type RedirectChannel struct {
	channel         chan interface {}
	timeoutDuration time.Duration
	id int//TODO: remove me after debug
}

// Redirect input one by one. Speed is O(Sum(R1 * RT1, ...)) dependents with R being the number of redirection
//  and RT it's timeout duration.
// quit: channel to use to tell the channelSplitter to exit. When triggered, channelSplitter will reply before leaving if someone is listening.
// input: channel to listen to redirect.
// redirects: list of redirections containing a channel and a timeout duration. First in, first redirection tried.
func channelSplitter(quit chan int, input chan interface{}, redirects ...*RedirectChannel) {
	waitgroup := sync.WaitGroup{}
	sleeper := func (output chan bool, d time.Duration){
		waitgroup.Add(1)
		time.Sleep(d)
		select {
		case output <- true:
		default:
		}
		waitgroup.Done()
	}

	splitterLoop:
	for {
		select {
		case in, status := <-input:
			if status != true {
				log.Print("ChannelSplitter: Input closed.")
				break splitterLoop
			}
			timeoutChan := make(chan bool)
			redirectsLoop:
			for _, redirect := range redirects {
				log.Printf("ChannelSplitter: Trying redirect %d.", redirect.id)
				go sleeper(timeoutChan, redirect.timeoutDuration)
				select {
				case redirect.channel <- in:
					break redirectsLoop
				case <-timeoutChan:
				case <-quit:
					break splitterLoop
				}
			}
		case <-quit:
			break splitterLoop
		}
	}
	log.Print("ChannelSplitter: waiting for remaining go routine.")
	waitgroup.Wait()
	quit <- 0
	log.Print("ChannelSplitter: quit.")
}