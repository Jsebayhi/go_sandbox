package main

import (
	"time"
	"fmt"
	"sync"
	"log"
	"testing"
)

const (
	ChannelClosed bool = false
)
var success bool = true

func basicTestingService(output chan int, redirect RedirectChannel, id int, waitgroup *sync.WaitGroup, expectedResult []int, breakNb int) {
	waitgroup.Add(1)
	received := []int{}
	loop:
	for i := 0; ; i++ {
		if i == breakNb {
			break loop
		}
		got, status := <-redirect.channel
		if status == ChannelClosed {
			break loop
		}
		log.Printf("redirect%d received %d.", id, got.(int))
		received = append(received, got.(int))
	}
	log.Printf("testingService %d - received: %s, expected %s.", id, fmt.Sprint(received), fmt.Sprint(expectedResult))
	if !intSliceComparison(received, expectedResult) {
		success = success && false
	}
	log.Printf("testingService %d - Stopping.", id)
	output <- 0
	waitgroup.Done()
}

func TestChannelSplitter(T *testing.T) {
	log.SetFlags(log.Ldate|log.Lmicroseconds)
	waitgroup := sync.WaitGroup{}

	success := true

	redirect1 := RedirectChannel{channel: make(chan interface{}), timeoutDuration: 1 * time.Second, id: 1}
	redirect2 := RedirectChannel{channel: make(chan interface{}), timeoutDuration: 2 * time.Second, id: 2}
	redirect3 := RedirectChannel{channel: make(chan interface{}), timeoutDuration: 3 * time.Second, id: 3}
	inputChannel := make(chan interface{})
	testServiceOutput := make(chan int)
	quitChannel := make(chan int)

	inputExpected_Redirect1 := []int{0, 1, 2}
	inputExpected_Redirect2 := []int{3, 4}
	inputExpected_Redirect3 := []int{5, 6}
	inputContent := append(inputExpected_Redirect1, inputExpected_Redirect2...)
	inputContent = append(inputContent, inputExpected_Redirect3...)

	go basicTestingService(testServiceOutput, redirect1, 1, &waitgroup, inputExpected_Redirect1, 3)
	go basicTestingService(testServiceOutput, redirect2, 2, &waitgroup, inputExpected_Redirect2, 2)
	go basicTestingService(testServiceOutput, redirect3, 3, &waitgroup, inputExpected_Redirect3, 10)

	go channelSplitter(quitChannel, inputChannel, &redirect1, &redirect2, &redirect3)

	//input feeder
	for input := range inputContent {
		log.Printf("Sending input: %d.", input)
		inputChannel <- input
	}
	log.Print("Closing input.")
	close(inputChannel)
	<-quitChannel
	close(redirect1.channel)
	close(redirect2.channel)
	close(redirect3.channel)
	<-testServiceOutput
	<-testServiceOutput
	<-testServiceOutput
	waitgroup.Wait()
	if success == false {
		T.Fatal("Failed.")
	} else {
		T.Log("Succes.")
	}
}

func intSliceComparison(s1, s2 []int) bool {
	if len(s1) != len(s2) {
		return false
	}
	for i := 0; i < len(s1); i++ {
		if s1[i] != s2[i] {
			return false
		}
	}
	return true
}