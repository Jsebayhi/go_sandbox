package main

import (
    "fmt"
    "time"
)

func	fnc() {
	fmt.Printf("fnc: Hello, you good?\n")
	fmt.Printf("fnc: out\n")
}

func	main() {
	fmt.Printf("main: launching goRoutine\n")
	go fnc()
	fmt.Printf("main waiting some seconds\n")
	time.Sleep(100 * time.Millisecond)
	fmt.Printf("main: time's up!\n")
	fmt.Printf("main: out\n")
}