package main

import (
	"time"
	"math/rand"
	"sync"
	"log"
	"strconv"
	"bufio"
	"os"
	"fmt"
)

var (
	waitgroup sync.WaitGroup
)

type Order struct {
	contact Person
}

type Mail struct {
	From Person
	To Person
}

type Die struct {}

type Person string
func (person Person) String() string {
	return string(person)
}

func PersonSoul(name string, addressBook AddressBook) {
	waitgroup.Add(1)
	mySelf := Person(name)
	myChannel := addressBook.Register(mySelf)
	defer func () {
		addressBook.UnRegister(mySelf)
		waitgroup.Done()
	}()
	life:
	for {
		message := <-myChannel
		switch message.(type) {
		case Order:
			outsideChannel := addressBook.Address(message.(Order).contact)
			outsideChannel <- Mail{From: mySelf, To: message.(Order).contact}
		case Mail:
			log.Printf("Person reporting: %s. Received message from %s.", mySelf.String(), message.(Mail).From.String())
		case Die:
			log.Printf("Person reporting: %s. Stopping.", mySelf.String())
			break life
		}
	}
}

// will give orders to people.
func Commander(addressBook AddressBook) {
	waitgroup.Add(1)
	defer waitgroup.Done()
	life:
	for {
		time.Sleep(3 * time.Second)
		if len(addressBook.Addresses()) == 1 {
			for _, address := range addressBook.Addresses() {
				address <- Die{}
			}
		}
		if len(addressBook.Addresses()) == 0 {
			log.Println("Commander reporting: Not enough people alive. Stopping.")
			break life
		}
		sender, senderChannel := pickTarget(addressBook.Addresses())
		target, _ := pickTarget(removeFromMap(addressBook.Addresses(), sender))
		log.Printf("Commander reporting: Commanding %s to send a message to %s.", sender.String(), target.String())
		senderChannel <- Order{contact: target}
	}
}

// Core
func init() {
	log.SetFlags(log.Ldate|log.Lmicroseconds)
}

func main() {
	addressBook := AddressBookNew()
	for i := 0; i < 10; i++ {
		go PersonSoul("player"+strconv.Itoa(i), addressBook)
	}
	go Commander(addressBook)
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Enter anything to quit")
	_, _ = reader.ReadString('\n')
	for _, address := range addressBook.Addresses() {
		address <- Die{}
	}
	waitgroup.Wait()
}

// tools

func removeFromMap(m map[Person]chan interface{}, rm Person) map[Person] chan interface{} {
	newMap := map[Person]chan interface{}{}
	for person, channel := range m {
		if person != rm {
			newMap[person] = channel
		}
	}
	return newMap
}

func	pickTarget(addresses map[Person]chan interface{}) (Person, chan interface{}) {
	personId := rand.Intn(len(addresses))
	i := 0
	for person, address := range addresses {
		if i == personId {
			return person, address
		}
		i++
	}
	log.Panic(i, personId)
	return Person(""), nil
}