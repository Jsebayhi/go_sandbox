package main

import (
	"sync"
	"log"
)

func AddressBookNew() AddressBook {
	return &addressBook{addresses: map[Person]chan interface{}{}}
}

type AddressBook interface {
	Addresses() map[Person]chan interface{}
	Address(person Person) chan interface{}
	Register(Person) chan interface{}
	UnRegister(Person)
}

type addressBook struct {
	addressesAccess   sync.Mutex
	addresses         map[Person]chan interface{}
}

func (ab *addressBook)Register(person Person) chan interface{} {
	log.Printf("AddressBook - registering new Person: %s.", person.String())
	newMailBox := make(chan interface{})

	ab.addressesAccess.Lock()
	for p := range ab.addresses {
		if person == p {
			return nil
		}
	}
	ab.addresses[person] = newMailBox
	ab.addressesAccess.Unlock()
	return newMailBox
}

func (ab *addressBook)UnRegister(person Person) {
	log.Printf("AddressBook - deleting Person: %s.", person.String())
	ab.addressesAccess.Lock()
	close(ab.addresses[person])
	delete(ab.addresses, person)
	ab.addressesAccess.Unlock()
}

func (ab addressBook)Address(person Person) chan interface{} {
	return ab.addresses[person]
}

func (ab addressBook)Addresses() map[Person]chan interface{} {
	return ab.addresses
}
