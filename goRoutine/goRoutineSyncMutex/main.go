package main

import (
	"log"
	"time"
	"math/rand"
	"sync"
)

func starter(id int) {
	log.Printf("Launcher - entering: %d.", id)
	mux.Lock()
	execTime := time.Duration(rand.Intn(10))
	log.Printf("Launcher - starting: %d for %d sec.", id, execTime)
	time.Sleep(execTime * time.Second)
	mux.Unlock()
	log.Printf("Launcher - leaving: %d.", id)
}

func routine(id int) {
	log.Printf("Routine - running: %d.", id)
	time.Sleep(1 * time.Second)
	//for {
		starter(id)
		time.Sleep(2 * time.Second)
	//}
	waitRoutine.Done()
}

var (
	mux sync.Mutex
	waitRoutine sync.WaitGroup
)

func main() {
	log.SetFlags(log.Ldate|log.Lmicroseconds|log.LUTC)
	for i := 0; i < 10; i++ {
		waitRoutine.Add(1)
		go routine(i)
	}
	waitRoutine.Wait()
}