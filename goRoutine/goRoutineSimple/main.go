package main

import (
    "fmt"
    "time"
)

func	fnc() {
	fmt.Printf("fnc: Hello, you good? please take a sit, I will take few seconds.\n")
	time.Sleep(100 * time.Millisecond)
	fmt.Printf("fnc: out\n")
}

func	main() {
	fmt.Printf("main: launching goRoutine\n")
	go fnc()
	time.Sleep(10 * time.Millisecond)
	fmt.Printf("main: Not waiting for anyone!\n")
	fmt.Printf("main: out\n")
}