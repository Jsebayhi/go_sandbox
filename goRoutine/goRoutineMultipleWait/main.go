package main

import (
    "fmt"
    "sync"
)

func	routineStart(waitRoutine *sync.WaitGroup, nb int) {
	fmt.Print("main: Setting waitRoutine\n")
	waitRoutine.Add(1)
	fmt.Print("main: launching goRoutine\n")
	go fnc(waitRoutine, nb)
}

func	fnc(waitRoutine *sync.WaitGroup, number int) {
	defer waitRoutine.Done()
	fmt.Printf("fnc %d: Hello, you good?\n", number)
	fmt.Printf("fnc %d: out\n", number)
}

func	main() {
	var waitRoutine sync.WaitGroup
	nb := 0

	nb++
	routineStart(&waitRoutine, nb)

	nb++
	routineStart(&waitRoutine, nb)

	nb++
	routineStart(&waitRoutine, nb)

	nb++
	routineStart(&waitRoutine, nb)

	fmt.Print("main waiting for Routines to be finished\n")
	waitRoutine.Wait()
	fmt.Print("main: goRoutine have finished\n")
	fmt.Print("main: out\n")
}