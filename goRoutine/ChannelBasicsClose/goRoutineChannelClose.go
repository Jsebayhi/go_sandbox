package main

import (
	"sync"
	"log"
	"fmt"
	"bufio"
	"os"
	"time"
)

func Listener(waitgroup *sync.WaitGroup, id int, input chan interface{}) {
	waitgroup.Add(1)
	log.Printf("Listener %d: waiting input channel report.", id)
	_, isOpen := <-input
	log.Printf("Listener %d: channel status: %v.", id, isOpen)
	waitgroup.Done()
}

func main() {
	waitGroup := &sync.WaitGroup{}
	input := make(chan interface{})
	close(input)
	listenerLastId := 0

	for listenerLastId < 5 {
		go Listener(waitGroup, listenerLastId, input)
		listenerLastId++
	}
	time.Sleep(1 * time.Second)

	input = make(chan interface{})
	for listenerLastId < 10 {
		go Listener(waitGroup, listenerLastId, input)
		listenerLastId++
	}
	time.Sleep(1 * time.Second)
	close(input)
	waitGroup.Wait()

	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Enter anything to quit")
	_, _ = reader.ReadString('\n')
}
