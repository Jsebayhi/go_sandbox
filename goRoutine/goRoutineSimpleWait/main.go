package main

import (
    "fmt"
    "sync"
)

func	fnc(waitRoutine *sync.WaitGroup) {
	defer waitRoutine.Done()
	fmt.Print("fnc: Hello, you good?\n")
	fmt.Print("fnc: out\n")
}

func	main() {
	var waitRoutine sync.WaitGroup


	fmt.Print("main: Setting waitRoutine\n")
	waitRoutine.Add(1)

	fmt.Print("main: launching goRoutine\n")
	go fnc(&waitRoutine)
	fmt.Print("main waiting for it to be finished\n")
	waitRoutine.Wait()
	fmt.Print("main: time's up!\n")
	fmt.Print("main: out\n")
}