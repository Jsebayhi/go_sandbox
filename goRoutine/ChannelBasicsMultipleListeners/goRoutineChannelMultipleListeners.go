package main

import (
	"log"
	"sync"
	"bufio"
	"os"
	"fmt"
	"time"
)

var (
	waitgroup = sync.WaitGroup{}
)

func Listener(id int, channel chan int) {
	waitgroup.Add(1)
	log.Printf("Listener %d - listenning for channel %v.", id, channel)
	for got := range channel {
		log.Printf("Listener %d - received: %d.", id, got)
	}
	waitgroup.Done()
}

func main() {
	channel := make(chan int)
	log.Printf("Channel address: %v.", channel)
	go Listener(0, channel)
	go Listener(1, channel)
	go Listener(2, channel)
	time.Sleep(1 * time.Second)

	log.Printf("Sending 1 through the channel %v.", channel)
	channel <- 1
	time.Sleep(1 * time.Second)

	log.Printf("Closing channel %v.", channel)
	close(channel)
	waitgroup.Wait()

	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Enter anything to quit")
	_, _ = reader.ReadString('\n')
}
