package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	//"os/signal"
	//"strings"
	"syscall"
	"time"
)

func myInit() {
	log.SetOutput(os.Stderr)
	log.SetPrefix("Taskmaster report: ")
}

func killAfterSleep(prc *os.Process, duration uint64) {
	dur := time.Duration(duration)
	fmt.Println("Attendons %d sec", dur)
	time.Sleep(dur * time.Second)
	prc.Signal(syscall.SIGKILL)
}

/*
func SignalHandler(prc *Process) {
	for {
		sig := (<-signal.Incoming).(signal.UnixSignal)
		switch {
		case sig == syscall.SIGINT || sig == syscall.SIGTERM:
			log.Printf("received %s - exiting", sig)
			prc.Signal(sig)
		case sig == syscall.SIGHUP:
			log.Printf("received %s - restarting", sig)
		case sig == syscall.SIGINFO:
			log.Printf("memory: %dMB alloc %dMB total",
				runtime.MemStats.Alloc/(1<<20),
				runtime.MemStats.TotalAlloc/(1<<20))
		default:
			log.Print(sig)
		}
	}
}
*/

func main() {
	var tab []string

	tab = nil
	if len(os.Args) > 1 {
		path, err := exec.LookPath(os.Args[1])
		if err != nil {
			log.Fatal(err)
		}
		lenTab := len(os.Args) - 1
		tab = make([]string, lenTab)
		for i := 0; i < lenTab; i++ {
			tab[i] = os.Args[i+1]
		}
		tab[0] = path
	} else {
		tab = []string{"/bin/ls", "-lR", "/"}
	}
	attr := new(os.ProcAttr)
	attr.Files = []*os.File{os.Stdin, os.Stdout, os.Stderr} // comprends pas pourquoi il n'herite pas des fds standards…
	prc, err := os.StartProcess(tab[0], tab, attr)
	if err != nil {
		log.Fatal(err)
	}
	killAfterSleep(prc, 2)
}
