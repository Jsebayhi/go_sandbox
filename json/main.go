package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
)

type TaskSettings struct {
	Name         string   `json:Name`
	Cmd          string   `json:cmd`
	Umask        int      `json:umask`
	Numprocs     int      `json:numprocs`
	Workingdir   string   `json:workingdir`
	Autostart    bool     `json:autostart`
	Autorestart  string   `json:autorestart`
	Exitcodes    []int    `json:exitcodes`
	Startretries int      `json:startretries`
	Stopsignal   string   `json:stopsignal`
	Stdout       string   `json:stdout`
	Stderr       string   `json:stderr`
	Env          []string `json:env`
}

var confPath string

func init() {
	const (
		defaultConf = "conf.json"
		usage       = "-c/--configuration FILENAME -- configuration file path (default conf.json)"
	)
	flag.StringVar(&confPath, "-configuration", defaultConf, usage)
	flag.StringVar(&confPath, "c", defaultConf, usage)
	flag.Parse()
}

func main() {
	var settings []TaskSettings

	conf, err := os.Open(confPath)
	if err != nil {
		log.Fatal(err)
	}
	defer conf.Close()
	dec := json.NewDecoder(conf)
	for {
		newEntry := new(TaskSettings)
		/*
			newEntry = {
				Numprocs:    1,
				Autostart:   false,
				Autorestart: "NEVER",
				Stopsignal:  "KILL",
				Stdout:      "/dev/null",
				Stderr:      "/dev/null",
				Env:         nil,
			}
		*/
		err := dec.Decode(&newEntry)
		if err == io.EOF {
			break
		} else if err != nil {
			log.Fatal(err)
		}
		fmt.Println(*newEntry)
		settings = append(settings, *newEntry)
	}
}
