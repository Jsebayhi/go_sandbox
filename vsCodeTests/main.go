package main

import (
	"errors"
	"fmt"
	"log"
	"os"
	"strings"
)

func addEnddingLineFeed(s string) (string, error) {
	if strings.LastIndexByte(s, '\n') != len(s)-1 {
		return s + "\n", nil
	}
	err := errors.New("has already an endding '\\n'")
	return "", err
}

func main() {
	for _, v := range os.Args[1:] {
		ret, err := addEnddingLineFeed(v)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Printf("v: %s\tret: %s\n", v, ret)
	}
}
