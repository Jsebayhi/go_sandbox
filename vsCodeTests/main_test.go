package main

import "testing"

func Test_addEnddingLineFeed(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name:    "toto",
			args:    args{s: "toto"},
			want:    "toto\n",
			wantErr: false,
		},
		{
			name:    "tata",
			args:    args{s: "tata"},
			want:    "tata\n",
			wantErr: false,
		},
		{
			name:    "to\nto",
			args:    args{s: "to\nto"},
			want:    "to\nto\n",
			wantErr: false,
		},
		{
			name:    "\n",
			args:    args{s: "\n"},
			want:    "",
			wantErr: true,
		},
		{
			name:    "toto\n",
			args:    args{s: "toto\n"},
			want:    "",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := addEnddingLineFeed(tt.args.s)
			if (err != nil) != tt.wantErr {
				t.Errorf("addEnddingLineFeed() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("addEnddingLineFeed() = %v, want %v", got, tt.want)
			}
		})
	}
}
