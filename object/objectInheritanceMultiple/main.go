package main

import (
	"fmt"
)

type S_family_father struct {

	name_last string

}

type S_family_mother struct {

	name_last string

}

/*
** inherit from S_family
*/
type S_person struct {
	
	S_family_father
	S_family_mother

	name_first string
}

func	main() {

	s_family_f := S_family_father{"father"}
	s_family_m := S_family_mother{"mother"}
	s_member := S_person{s_family_f, s_family_m, "toto"}

	
	fmt.Printf("basic inheritance: %+v\n", s_member)

	//family.name_last = "New Name"
	//fmt.Printf("Changing family last name. Here is member:\n%+v\n", s_member)
}