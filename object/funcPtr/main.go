package main

import (
	"fmt"
)

type Struct struct {
	name    string
	funcPtr func(s *Struct) *Struct
}

func StructConstructor(name string, funcPtr func(s *Struct) *Struct) *Struct {
	s := Struct{name, funcPtr}
	if s.funcPtr == nil {
		s.funcPtr = func(s *Struct) *Struct {
			return s
		}
	}
	return &s
}

func Print(s *Struct) *Struct {
	fmt.Println("name: " + s.name)
	return s
}

func Print2(s interface{}) {
	fmt.Println(s)
}

func main() {

	Struct := StructConstructor("MyName", nil)

	ret := Struct.funcPtr(Struct)
	Print2(ret)
}
