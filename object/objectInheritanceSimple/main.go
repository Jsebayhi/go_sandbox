package main

import (
	"fmt"
)

type S_family struct {

	name_last string

}

/*
** inherit from S_family
*/
type S_person struct {
	
	S_family
	name_first string
}

func	main() {

	s_family := S_family{"family"}
	s_member := S_person{s_family, "toto"}

	
	fmt.Printf("basic inheritance:\n%+v\n\n", s_member)

	s_family.name_last = "family_1"
	fmt.Printf("Changing family last name. Here is member (shouldn't change):\n%+v\n\n", s_member)
	
	s_member_2 := S_person{S_family{"family_2"}, "Junior"}
	fmt.Printf("Otherway to create son. Result:\n%+v\n\n", s_member_2)


}