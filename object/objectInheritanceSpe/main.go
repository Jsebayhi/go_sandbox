package main

import (
	"fmt"
)

type S_container struct {

	name_last string

}

/*
** inherit from S_container
*/
type S_object struct {
	
	S_container
	S_object
}

func	main() {

	s_container := S_container{"family"}
	s_object := S_object{s_container, nil}

	
	fmt.Printf("first S_object:\n%+v\n\n", s_object)

	s_object_2 := S_object_2{nil, s_object}
	fmt.Printf("Changing family last name. Here is member (shouldn't change):\n%+v\n\n", s_object_2)

}