package main

import (
	"fmt"
)

type Struct struct {
	name_last string
}

func (s *Struct) Print(msg string) {
	fmt.Println("method: " + msg)
}

func Print2(msg string) {
	fmt.Println("Out: " + msg)
}

func main() {

	Struct := Struct{"family"}

	Struct.Print("toto")
	Struct.Print = Print2
	Struct.Print("toto")
}
