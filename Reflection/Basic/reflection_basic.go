package main

import (
	"fmt"
	"reflect"
)

func goReflect(a interface{}) {

	fmt.Printf("here is a %+v\n", a)
	fmt.Printf("here is type of a %+v\n", reflect.TypeOf(a))

	b := reflect.ValueOf(a)
	b = reflect.ValueOf("toto")
	if !reflect.DeepEqual(reflect.ValueOf(a), b) {
		fmt.Printf("failed: here is type of a %+v and it's content %+v\n", reflect.TypeOf(a), a)
		fmt.Printf("failed: here is type of b %+v and it's content %+v\n", reflect.TypeOf(b), b)
	} else {
		fmt.Printf("success: here is type of b %+v and it's content %+v\n", reflect.TypeOf(b), b)
	}
}

func main() {

	var a string

	a = "toto"
	goReflect(a)
}
