package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"reflect"
)

type fixtureType1 struct {
	Name string
}

var fixture fixtureType1 = fixtureType1{Name: "toto"}

func try(ref interface{}) {

	fixtureJSON, err := json.Marshal(fixture)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("fixtureJSON: %+v\n", fixtureJSON)

	t := reflect.TypeOf(ref)
	v := reflect.New(t)

	if err := json.NewDecoder(bytes.NewReader(fixtureJSON)).Decode(v.Interface()); err != nil {
		fmt.Println(err)
	}
	// comparison between ref and received
	if reflect.DeepEqual(v.Elem().Interface(), ref) {
		fmt.Println("OK")
	} else {
		fmt.Println("KO")
		fmt.Printf("v: %+v\n", v.Elem().Interface())
		fmt.Printf("ref: %+v\n", ref)
	}
}

func main() {
	fmt.Printf("fixture: %+v\n", fixture)
	try(&fixture)
}
